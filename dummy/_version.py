"""Version management module. Put next to __init__.py to provide a version for."""
import logging
import sys
from pathlib import Path
from subprocess import CalledProcessError, check_output
from textwrap import dedent
from typing import Optional

import semver

__version__ = str(semver.VersionInfo(1, 0, 0))

logger = logging.getLogger(__file__)


def get_version(
    git_root: str = "..",
    git_tag_prefix: str = "v",
    version_file: str = "VERSION",
    allow_fallback: bool = True,
    fallback_version: semver.VersionInfo = semver.VersionInfo(0, 0, 0, None, 0),
) -> semver.VersionInfo:
    """Get the version of this package.

    Will try to obtain a semantic version (see `semver` package) in the following order:
      1. From a git tag. See the `from_git` function in this module.
      2. From a version. See the `from_file` function in this module.
      3. Return `0.0.0+0` as a fallback version.

    Arguments:
        git_root: Git root directory.
        git_tag_prefix: Prefix of git tags. (E.g. the 'v' in 'v1.0.0'.)
        version_file: Version file.
        allow_fallback: Whether to allow the fallback version option.
        fallback_version: The actual fallback version.

    Returns:
        Semantic version.

    Note:
        Relative paths are interpreted w.r.t. this module's location.
    """
    try:
        git_version = _from_git(git_root, git_tag_prefix)
    except Exception:
        logger.debug("Could not find a suitable git tag version.", exc_info=True)
        git_version = fallback_version

    try:
        file_version = _from_file(version_file)
    except Exception:
        logger.debug("Could not find a suitable version file version.", exc_info=True)
        file_version = fallback_version

    version = max(git_version, file_version)
    logger.debug(
        "Picked {} from (git: {}) and (file: {}).".format(
            version, git_version, file_version
        )
    )

    if version != fallback_version:
        return version
    elif allow_fallback:
        msg = "No package version found, falling back to '{}'.".format(fallback_version)
        logger.warning(msg)
        return version
    else:
        msg = "Cannot find any suitable version from git root {} or file {}.".format(
            git_root, version_file
        )
        logger.error(msg)
        raise ValueError(msg)


def _from_file(version_file: str) -> semver.VersionInfo:
    """Get a version from file."""
    logger.debug("Getting version from version file '{}'.".format(version_file))
    version_file = _resolve_path(version_file)
    version = semver.parse_version_info(version_file.read_text())
    logger.debug("Found file version '{}'.".format(version))
    return version


def _from_git(git_root: str, git_tag_prefix: str = "v") -> semver.VersionInfo:
    """Get a semantic version from a git tag.

    Arguments:
        git_root: Git root directory.
        git_tag_prefix: Prefix of git tags. (E.g. the 'v' in 'v1.0.0'.)

    Returns:
        Semantic version.
    """
    logger.debug("Getting version from git root '{}'...".format(git_root))
    git_root = _resolve_path(git_root)

    try:
        output = (
            check_output(
                [
                    "git",
                    "describe",
                    "--tags",
                    "--dirty",
                    "--always",
                    "--long",
                    "--match",
                    "*.*",
                ],
                cwd=str(git_root),
                encoding="utf-8",
            )
            .strip()
            .lstrip("v")
        )
    except CalledProcessError as excinfo:
        logger.debug(excinfo.stderr, exc_info=True)

    logger.debug("Parsing output '{}'.".format(output))
    parts = output.split("-")

    if len(parts) == 3:
        version, ahead, sha = parts
        build = ".".join([ahead, sha])
    elif len(parts) == 4:
        version, ahead, sha, dirty = parts
        build = ".".join([ahead, sha, dirty])
    else:
        raise ValueError("Cannot parse git tag {}.".format(output))

    # Expand x.x to x.x.0
    if len(version.split(".")) == 2:
        version += ".0"

    # x.x.x+{ahead}.{sha}.{dirty}
    to_parse = version + "+" + build
    logger.debug("Handing '{}' to semver to parse.".format(to_parse))

    version = semver.parse_version_info(to_parse)
    logger.debug("Found git version '{}'.".format(version))

    return version


def bake_version(
    version: Optional[semver.VersionInfo] = None,
    git_root: Optional[str] = None,
    version_file: Optional[str] = None,
    force: bool = False,
) -> semver.VersionInfo:
    """Bake version to git and/or file, depending on arguments.

    Arguments:
        version: Version to bake, defaults to result of `get_version()`.
        git_root: Git root directory to bake a version tag in (if applicable).
        version_file: Version file to bake a version to (if applicable).
        force: Whether to bake version even if dirty or ahead.

    Note:
        Either `git_root` or `version_file` must be not `None`. Paths should either be
        relative to this module or absolute.
    """
    if not git_root and not version_file:
        raise ValueError("Either `git_root` or `version_file` must be not `None`.")

    if not version:
        version = get_version(allow_fallback=False)

    logger.debug("Baking version '{}'.".format(version))

    # Check whether version is either ahead or dirty
    dirty = version.build.contains("dirty")
    try:
        ahead = int(version.build.split(".")[0])
    except Exception:
        logger.debug("Could not determine commits ahead.", exc_info=True)
        ahead = False

    if not force and (dirty or ahead):
        msg = dedent(
            "Cannot bake version {}. Dirty: {}. Ahead: {}. Use `force=True` to \
            override.".format(
                version, dirty, ahead
            )
        )
        logger.error(msg)
        raise ValueError(msg)

    if git_root:
        _bake_git(version, git_root)

    if version_file:
        _bake_file(version, version_file)


def _bake_file(version: semver.VersionInfo, version_file: str):
    """Bake version to file.

    Arguments:
        version: Version to bake.
        version_file: Version file to bake a version to.
    """
    version_file = _resolve_path(version_file)
    logger.debug("Baking version '{}' to file '{}'...".format(version, version_file))
    version_file.write_text(version)
    logger.debug("Baked file.")


def _bake_git(
    version: semver.VersionInfo,
    git_root: str,
    git_tag_prefix: str,
    msg: Optional[str] = None,
):
    """Bake version to git tag.

    Arguments:
        version: Version to bake.
        git_root: Git root directory to bake a version tag in.
        git_tag_prefix: Prefix of git tags. (E.g. the 'v' in 'v1.0.0'.)
        msg: Optional git tag annotation message. Defaults to 'Version x.x.x'.
    """
    git_root = _resolve_path(git_root)
    logger.debug("Baking version '{}' to git_root '{}'...".format(version, git_root))

    sv = semver.finalize_version(version)
    tag = git_tag_prefix + sv
    if not msg:
        msg = "Version " + sv

    check_output(["git", "tag", "-a", tag, "-m", '"{}"'.format(msg)])
    logger.debug("Baked to tag '{}' with message '{}'.".format(tag, msg))


def _resolve_path(path: str) -> Path:
    """Resolve relative paths as paths relative to this module's location."""
    if not Path(path).is_absolute():
        return Path(__file__).parent / path
    else:
        return path


if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler(sys.stdout))
    get_version()
