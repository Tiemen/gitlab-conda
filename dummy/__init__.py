"""dummy

A dummy project, versioned by git tags, and hosting it's conda package using Gitlab pages.
"""

from . import _version

__version__ = str(_version.get_version())
