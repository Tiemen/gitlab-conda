@ECHO OFF

pushd %~dp0

if "%1" == "" goto help
goto %1

:help
echo.Try one of the following targets as 'make ^<target^>':
echo.build conda install test doc
goto end

:wheel
flit build --format wheel
goto end

:conda
conda build ./conda.recipe --output-folder ./dist
conda index ./dist
goto end

:install
flit install
goto end

:test
pushd tests
py.test --cov=dummy --cov-report term-missing --junitxml=./.pytest_cache/pytest.xml
goto end

:doc
if exist ".\doc\source\reference\" rd /q /s ".\doc\source\reference\"
sphinx-apidoc -MTfo ./doc/source/reference ./dummy
./doc/make.bat html
goto end

:end
popd