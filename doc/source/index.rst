.. include:: ../../README.rst

Documentation Contents
----------------------

.. toctree::
   :caption: Introduction
   :maxdepth: 2


.. toctree::
   :caption: Appendix
   :maxdepth: 2

   Reference <./reference/dummy>
   Index <genindex>
