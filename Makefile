.PHONY: help wheel conda install test doc Makefile

# show some help
help:
	@echo "Try one of the following targets as 'make <target>:"
	@echo "wheel conda install test doc"

wheel:
	flit build --format wheel

conda:
	conda build ./conda.recipe --output-folder ./dist
	conda index ./dist

install:
	flit install

test:
	cd tests && py.test --cov=dummy --cov-report term-missing --junitxml=./.pytest_cache/pytest.xml

doc:
	rm -rf ./doc/source/reference
	sphinx-apidoc -MTfo ./doc/source/reference ./dummy
	cd doc && make html
